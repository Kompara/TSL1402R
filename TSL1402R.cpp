/*
 * TSL1402R
 *  This is the main file of TSL1402R library.
 *
 * @file TSL1402R.cpp
 * @author Tomaz Kompara <tomaz@kompara.si>
 * @date 7.7.2016
 * @copyright (c) 2016 Tomaz Kompara
 * 
 * Released under GNU General Public License v2.0
 * 
 * You may copy, distribute and modify the software as long as you track 
 * changes/dates in source files. Any modifications to or software including 
 * (via compiler) GPL-licensed code must also be made available under the GPL 
 * along with build & install instructions.
 */

#include "TSL1402R.h"
#include <TSL1402R.h>

// Define various ADC prescaler:
const unsigned char PS_32  = (1 << ADPS2) | (1 << ADPS0);
const unsigned char PS_128 = (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);


TSL1402R::TSL1402R(byte _SI, byte _CLK, byte _A0, byte _A1){
  SI   = _SI;
  CLK  = _CLK;
  ADC0 = _A0;
  ADC1 = _A1;

  SI_mask = digitalPinToBitMask(_SI);
  CLK_mask = digitalPinToBitMask(_CLK);
}

void TSL1402R::begin(){
  begin(0.0001);
}

void TSL1402R::begin(float _SHUTTER){
  // Convert seconds to microseconds
  SHUTTER = (int)(_SHUTTER * 1000.0 * 1000.0);

  // Initialize two Arduino pins as digital output:
  pinMode(CLK, OUTPUT);
  pinMode(SI, OUTPUT);

  // To set up the ADC, first remove bits set by Arduino library, then choose 
  // a prescaler: PS_16, PS_32, PS_64 or PS_128:
  ADCSRA &= ~PS_128;  
  ADCSRA |= PS_32; // <-- Using PS_32 makes a single ADC conversion take ~30 us

  analogReference(DEFAULT);

  for(int i=0;i< 130;i++) CLK_pulse();
  SI_CLK_pulse();
  for(int i=0;i< 130;i++) CLK_pulse();
}

void TSL1402R::read(int *pixel){
  unsigned long t1 = micros();
  SI_CLK_pulse();
  for(int i = 0; i < 130; i++) CLK_pulse();
  while(micros() < t1 + SHUTTER){} // wait declared time
  SI_CLK_pulse();

  for(int i=0; i < 128; i++){
    pixel[i]     = analogRead(ADC0);
    pixel[i+128] = analogRead(ADC1);
    CLK_pulse();
  }
}

// This function generates an outgoing clock pulse from the Arduino digital 
// pin 'CLKpin'. This clock pulse is fed into pin 3 of the linear sensor:

void TSL1402R::CLK_pulse(){
  PORTE |= CLK_mask;
  PORTE &= ~CLK_mask;
}

void TSL1402R::SI_CLK_pulse(){
  PORTE |= SI_mask;
  CLK_pulse();
  PORTE &= ~SI_mask;
}