/*
 * TSL1402R
 *  This is the header file of TSL1402R library.
 *
 * @file TSL1402R.h
 * @author Tomaz Kompara <tomaz@kompara.si>
 * @date 7.7.2016
 * @copyright (c) 2016 Tomaz Kompara
 * 
 * Released under GNU General Public License v2.0
 * 
 * You may copy, distribute and modify the software as long as you track 
 * changes/dates in source files. Any modifications to or software including 
 * (via compiler) GPL-licensed code must also be made available under the GPL 
 * along with build & install instructions.
 */

#ifndef TSL1402R_h
#define TSL1402R_h

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#include <TSL1402R.h>

class TSL1402R
{
  public:
    TSL1402R(byte _SI, byte _CLK, byte _A0, byte _A1);
    void begin();
    void begin(float _SHUTTER); // Shutter speed in seconds
    void read(int *pixel);

  private:
    void CLK_pulse();
    void SI_CLK_pulse();
    unsigned int readChannel(byte thisCh);
    int SHUTTER;  // Shutter speed in microseconds (default: 1000 us)
    byte SI;      //SI1 and SI2 (parallel mode)
    byte CLK;     //CLK
    byte SI_mask;
    byte CLK_mask;
    byte ADC0;    //AO1
    byte ADC1;    //AO2
};

#endif