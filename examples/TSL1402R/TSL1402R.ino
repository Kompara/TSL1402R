/*
 * TSL1402R
 *  This is a simple example of using TSL1402R library.
 *
 * @file TSL1402R.ino
 * @author Tomaz Kompara <tomaz@kompara.si>
 * @date 7.7.2016
 * @copyright (c) 2016 Tomaz Kompara
 * 
 * Released under GNU General Public License v2.0
 * 
 * You may copy, distribute and modify the software as long as you track 
 * changes/dates in source files. Any modifications to or software including 
 * (via compiler) GPL-licensed code must also be made available under the GPL 
 * along with build & install instructions.
 */

#include <TSL1402R.h>

TSL1402R TSL(5, 3, 0, 1); // SI, CLK, A0, A1
int readout[256]; // array for saving data from TSL1402R

void setup () {
  Serial.begin (115200);
  TSL.begin(1.0/5000);   // Begin the TSL1402R with 1/5000 shutter speed.
}

void loop () {
  // Read data from TSL1402R sensor
  TSL.read(readout);

  // Print data to serial
  for(int i = 0; i < 256; i++){
    Serial.print(readout[i]);
    Serial.print(","); 
  }
  Serial.println("");

  delay(200);
}