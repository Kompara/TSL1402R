# TSL1402R library

This is a simple arduino library that can work with TSL1402R (https://ams.com/jpn/content/download/250165/975693/file/TSL1402R_Datasheet_EN_v1.pdf).

## Installation
### Arduino
#### 'Static' install
Install the same as any other Arduino library.

1. Copy `arduino/libraries/TSL1402R` into `/{path-to-Arduino-folder}/Sketches/libraries`

In this case you will always need to rewrite files when you version is released. To make your life a bit easier checkout 'dynamic' installation.

#### 'Dynamic' installation
1. GIT-clone TSL1402R anywhere on your drive
2. Run this...
```
$ cd /{path-to-Arduino-folder}/Sketches/libraries
$ ln -s /{path-to-TSL1402R-folder}/arduino/libraries/TSL1402R TSL1402R
```

## Connecting
This is how to connect your Arduino to the TSL1402R in parallel mode.

|TSL1402R          |     Arduino    |
|:----------------:|:--------------:|
|(1)  Vdd          |3.3V / 5V       |
|(2)  SI1          |SI (any GPIO)   |
|(3)  CLK          |CLK (any GPIO)  |
|(4)  AO1          |A0 (any ANALOG) |
|(5)  GND          |GND             |
|(6)  SO2          |NC              |
|(7)  NC           |NC              |
|(8)  AO2          |A1 (any ANALOG) |
|(9)  NC           |NC              |
|(10) SI2          |SI (same as SI1)|
|(11) NC           |NC              |
|(12) GND          |GND             |
|(13) SO1          |NC              |
|(14) NC           |NC              |


## Usage
Run Arduino IDE or similar and open examples to see it working:

* TSL1402R.ino - Example of simple reading data from TSL1402R

## Licence
### Released under GNU General Public License v2.0
You may copy, distribute and modify the software as long as you track changes/dates in source files. Any modifications to or software including (via compiler) GPL-licensed code must also be made available under the GPL along with build & install instructions.